from django.apps import AppConfig


class SsoAuthConfig(AppConfig):
    name = 'sso_auth'
